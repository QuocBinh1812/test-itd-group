class MyBigNumber {
  constructor() {
    this.history = [];
  }

  sum(stn1, stn2) {
    let result = "";
    let memory = 0;
    let len1 = stn1.length;
    let len2 = stn2.length;
    for (let i = 1; i <= Math.max(len1, len2); i++) {
      let digit1 = Number(stn1[len1 - i]) || 0;
      let digit2 = Number(stn2[len2 - i]) || 0;
      let sum = digit1 + digit2 + memory;
      memory = Math.floor(sum / 10);
      result = (sum % 10) + result;
      this.history.push(
        `${digit1} + ${digit2} = ${sum}, memory = ${memory}, result = ${result}`
      );
    }
    memory > 0 ? (result += memory) : result;
    return Number(result).toString();
  }
}
const myBigNumber = new MyBigNumber();
console.log(myBigNumber.sum("1234", "9897"));
console.log(myBigNumber.history);
module.exports = MyBigNumber;
