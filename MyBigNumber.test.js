//import MyBigNumber from "./MyBigNumber";
const MyBigNumber = require("./MyBigNumber");
describe("MyBigNumber", () => {
  describe("sum", () => {
    test("should return the sum of two numbers", () => {
      const myBigNumber = new MyBigNumber();
      expect(myBigNumber.sum("1234", "897")).toBe("2131");
    });

    test("should return the sum of two numbers with different length", () => {
      const myBigNumber = new MyBigNumber();
      expect(myBigNumber.sum("1001", "98")).toBe("1099");
    });

    test("should return the sum of two numbers start with zeros", () => {
      const myBigNumber = new MyBigNumber();
      expect(myBigNumber.sum("00182", "40")).toBe("222");
    });

    test("should return the sum of two numbers with memory out ", () => {
      const myBigNumber = new MyBigNumber();
      expect(myBigNumber.sum("1234", "9897")).toBe("11311");
    });
  });
});
